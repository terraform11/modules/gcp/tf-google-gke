resource "google_container_cluster" "primary" {
    name = "${var.name}"
    location = "${var.location}"

    remove_default_node_pool = "${var.remove_default_node_pool}"
    initial_node_count       = "${var.initial_node_count}"

}
resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.name_pool}"
  location   = "${var.location}"
  cluster    = google_container_cluster.primary.name
  node_count = "${var.node_count}"
  node_config {
    preemptible  = true
    machine_type = "${var.machine_type}"
    metadata = {
      disable-legacy-endpoints = "${var.disable-legacy-endpoints}"
    }
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}