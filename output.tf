output "cluster" {
  value = google_container_cluster.default
}

output "node_pool_name" {
  value = "${google_container_node_pool.node_pool.name}"
}