variable "remove_default_node_pool" {
  description = "The action for remove the default node pool."
  type = bool
  default = true
}

variable "name" {
  description = "The cluster name."
}

variable "location" {
  description = "The location (region or zone) in which the cluster master will be created."
}

variable "initial_node_count" {
  description = "The minimum version of the master."
  type = number
  default = 1
}

variable "name_pool" {
  description = "The pool name."
}

variable "node_count" {
  description = "Node counts."
  type = number
  default = 1
}

variable "machine_type" {
    description = "Machine type"
}

variable "disable-legacy-endpoints" {
    description = "Disable the legacy endpoints"
    type = bool
    default = true
}

